/* Touch Screen */
#define CHIMERA_PAD98_PTB28_TS_IRQ			\
		IOMUX_PAD(0x0188, 0x0188, 0, 0x0000, 0, \
		MVF600_GPIO_GENERAL_CTRL | PAD_CTL_IBE_ENABLE)

#define MVF600_PAD29_PTB7_GPIO_29		\
	IOMUX_PAD(0x0074, 0x0074, 0, 0x0000, 0, MVF600_GPIO_GENERAL_CTRL | PAD_CTL_IBE_ENABLE)

/* Openframe LVDS reset */

#define CHIMERA_PAD65_PTD29_GPIO		\
	IOMUX_PAD(0x0104, 0x0104, 0, 0x0000, 0, MVF600_GPIO_GENERAL_CTRL | PAD_CTL_IBE_ENABLE)

#define CHIMERA_PAD68_PTD26_GPIO		\
	IOMUX_PAD(0x0110, 0x0110, 0, 0x0000, 0, MVF600_GPIO_GENERAL_CTRL | PAD_CTL_IBE_ENABLE)


#define CHIMERA_PAD12_PTA22_I2C2_SCL			\
		IOMUX_PAD(0x0030, 0x0030, 6, 0x033C, 1, \
				MVF600_I2C_PAD_CTRL | PAD_CTL_OBE_IBE_ENABLE)
#define CHIMERA_PAD13_PTA23_I2C2_SDA			\
		IOMUX_PAD(0x0034, 0x0034, 6, 0x033C, 1, \
				MVF600_I2C_PAD_CTRL | PAD_CTL_OBE_IBE_ENABLE)

#define CHIMERA_PAD132_PTE27_I2C1_SCL				\
		IOMUX_PAD(0x0210, 0x0210, 5, 0x033C, 1, MVF600_I2C_PAD_CTRL | PAD_CTL_OBE_IBE_ENABLE )
#define CHIMERA_PAD133_PTE28_I2C1_SDA				\
		IOMUX_PAD(0x0214, 0x0214, 5, 0x033C, 1, MVF600_I2C_PAD_CTRL | PAD_CTL_OBE_IBE_ENABLE )


#define CHIMERA_PAD33_PTB11_CKO2				\
		IOMUX_PAD(0x0084, 0x0084, 6, 0x0000, 0, \
				MVF600_DCU_PAD_CTRL | PAD_CTL_OBE_ENABLE)


#define CHIMERA_PAD62_PTC17_SAI2_TX_SYNC 	\
		IOMUX_PAD(0x00F8, 0x00F8, 5, 0x0374, 1, \
				MVF600_SAI_PAD_CTRL | PAD_CTL_IBE_ENABLE)


#define CHIMERA_PAD59_PTC14_SAI2_RX_DATA	\
		IOMUX_PAD(0x00EC, 0x00EC, 5, 0x0000, 0, \
				MVF600_SAI_PAD_CTRL | PAD_CTL_IBE_ENABLE)

#define CHIMERA_PAD60_PTC15_SAI2_TX_DATA 	\
		IOMUX_PAD(0x00F0, 0x00F0, 5, 0x0000, 0, \
				MVF600_SAI_PAD_CTRL | PAD_CTL_OBE_ENABLE)

#define CHIMERA_PAD6_PTA16_SAI2_TX_BCLK		\
		IOMUX_PAD(0x0018, 0x0018, 5, 0x0370, 0, \
				MVF600_SAI_PAD_CTRL | PAD_CTL_IBE_ENABLE)

#define	CHIMERA_PAD132_PTE27_I2C1_GPIO				\
		IOMUX_PAD(0x0210, 0x0210, 0, 0x033C, 1, MVF600_I2C_PAD_CTRL | PAD_CTL_OBE_IBE_ENABLE )
#define	CHIMERA_PAD133_PTE28_I2C1_GPIO				\
		IOMUX_PAD(0x0214, 0x0214, 0, 0x033C, 1, MVF600_I2C_PAD_CTRL | PAD_CTL_OBE_IBE_ENABLE )

#define	CHIMERA_PAD36_PTB14__CAN0_RX IOMUX_PAD(0x0090, 0x0090, 1, 0x0000, 0, 0)

#define	CHIMERA_PAD37_PTB15__CAN0_TX IOMUX_PAD(0x0094, 0x0094, 1, 0x0000, 0, 0)

/*UART*/
#define CHIMERA_PAD_PTA20_UART3_TX				\
		IOMUX_PAD(0x0028, 0x0028, 6, 0x0394, 0, \
				MVF600_UART_PAD_CTRL | PAD_CTL_OBE_ENABLE)
#define CHIMERA_PAD_PTA21_UART3_RX				\
		IOMUX_PAD(0x002C, 0x002C, 6, 0x0390, 0, \
				MVF600_UART_PAD_CTRL | PAD_CTL_IBE_ENABLE)

#define CHIMERA_PAD_PTD0_UART2_TX				\
		IOMUX_PAD(0x013C, 0x013C, 2, 0x038C, 2, \
				MVF600_UART_PAD_CTRL | PAD_CTL_OBE_ENABLE)
#define CHIMERA_PAD_PTD1_UART2_RX				\
		IOMUX_PAD(0x0140, 0x0140, 2, 0x0388, 2, \
				MVF600_UART_PAD_CTRL | PAD_CTL_IBE_ENABLE)



